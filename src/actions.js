const SYNC_START = 'SYNC_START',
    SYNC_FINISH = 'SYNC_FINISH',
    ADD_NOTE = 'ADD_NOTE',
    EDIT_NOTE = 'EDIT_NOTE',
    REMOVE_NOTE = 'REMOVE_NOTE',
    SYNC_NOTES = 'SYNC_NOTES',
    ADD_LABEL = 'ADD_LABEL',
    EDIT_LABEL = 'EDIT_LABEL',
    REMOVE_LABEL = 'REMOVE_LABEL',
    SYNC_LABELS = 'SYNC_LABELS'

export const actionTypes = {
    SYNC_FINISH,
    SYNC_START,
    ADD_NOTE,
    EDIT_NOTE,
    REMOVE_NOTE,
    SYNC_NOTES,
    ADD_LABEL,
    EDIT_LABEL,
    REMOVE_LABEL,
    SYNC_LABELS
};

export const syncStart = () => {
    return {
        type: SYNC_START
    };
};

export const syncFinish = () => {
    return {
        type: SYNC_FINISH
    };
};

export const notesSync = notes => {
    return {
        type: SYNC_NOTES,
        notes
    };
}
import { actionTypes } from './actions';
import { combineReducers } from 'redux';
const {
    SYNC_START,
    SYNC_FINISH,
    ADD_NOTE,
    EDIT_NOTE,
    REMOVE_NOTE,
    SYNC_NOTES,
    ADD_LABEL,
    EDIT_LABEL,
    REMOVE_LABEL,
    SYNC_LABELS
} = actionTypes;

const syncState = (state = false, action) => {
    switch (action.type) {
        case SYNC_START:
            return true;
        case SYNC_FINISH:
            return false;
        default:
            return state;
    }
};

const notes = (state = { data: [], lastUpdate: new Date() }, action) => {
    switch (action.type) {
        case ADD_NOTE:
            return {
                data: [
                    ...state.data,
                    action.note
                ],
                lastUpdate: new Date()
            };
        case EDIT_NOTE:
            return {
                data: state.data.map(note => 
                    note._id === action.note._id ? action.note : note),
                lastUpdate: new Date()
            };
        case REMOVE_NOTE:
            return {
                data: state.data.filter(note => note._id !== action.note._id),
                lastUpdate: new Date()
            };
        case SYNC_NOTES:
            return {
                data: action.notes,
                lastUpdate: new Date()
            };
        default:
            return state;
    }
};

const labels = (state = { data: [], lastUpdate: new Date() }, action) => {
    switch (action.type) {
        case ADD_LABEL:
            return {
                data: [
                    ...state.data,
                    action.label
                ],
                lastUpdate: new Date()
            };
        case EDIT_LABEL:
            return {
                data: state.data.map(label => 
                    label._id === action.label._id ? action.label : label),
                lastUpdate: new Date()
            };
        case REMOVE_LABEL:
            return {
                data: state.data.filter(label => label._id !== action.label._id),
                lastUpdate: new Date()
            };
        case SYNC_LABELS:
            return {
                data: action.labels,
                lastUpdate: new Date()
            };
        default:
            return state;
    }
};

export default combineReducers({
    syncState,
    notes,
    labels
});

export class App {
  constructor() {
    this.message = 'Welcome to the Notes App!';
  }
  configureRouter(config, router) {
    config.title = 'Notes App';
    config.map([
      { route: '', moduleId: 'components/home', title: 'Home' },
      { route: 'notes', moduleId: 'components/notes/list', name: 'notes-list' },
      { route: 'notes/:id', moduleId: 'components/notes/detail', name: 'notes-detail' },
      { route: 'notes/:id/edit', moduleId: 'components/notes/form', name: 'notes-form' },      
      { route: 'labels', moduleId: 'components/labels/list', name: 'labels-list' },
      { route: 'labels/:id', moduleId: 'components/labels/detail', name: 'labels-detail' },
      { route: 'labels/:id/edit', moduleId: 'components/labels/form', name: 'labels-form' }
    ]);

    this.router = router;
  }
}

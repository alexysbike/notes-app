import { HttpClient } from 'aurelia-http-client';
const client = new HttpClient()
    .configure(x => {
        x.withBaseUrl('http://localhost:10010');
        x.withInterceptor({
            request(message) {
                return message;
            },

            requestError(error) {
                throw error;
            },

            response(message) {
                message.json = JSON.parse(message.response);
                return message;
            },

            responseError(error) {
                throw error;
            }
        });
    });
export class LabelService {

    async getAll() {
        const response = await client.get('labels');
        return response.json.data;
    }

    async getById(id) {
        const response = await client.get(`labels/${id}`);
        return response.json.data;
    }

    async save(id, label) {
        let response;
        if (id) {
            response = await client.put(`labels/${id}`, label);
        } else {
            response = await client.post(`labels`, label);
        }
        return response.json.data;
    }
}

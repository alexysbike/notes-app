import {HttpClient} from 'aurelia-http-client';
import {inject} from 'aurelia-framework';
import Store from '../store';
import {notesSync} from '../actions';
const client = new HttpClient()
    .configure(x => {
        x.withBaseUrl('http://localhost:10010');
        x.withInterceptor({
            request(message) {
                return message;
            },

            requestError(error) {
                throw error;
            },

            response(message) {
                message.json = JSON.parse(message.response);
                return message;
            },

            responseError(error) {
                throw error;
            }
        });
    });
@inject(Store)
export class NoteService{

    constructor(store){
        this.store = store;
    }

    async syncData(){
        const response = await client.get('notes');
        this.store.dispatch(notesSync(response.json.data));
        return true;
    }

    async getAll(){
        const response = await client.get('notes');
        return response.json.data;
    }

    async getById(id){
        const response = await client.get(`notes/${id}?populate=true`);
        return response.json.data;
    }

    async save(id, note) {
        let response;
        if (id) {
            response = await client.put(`notes/${id}`, note);
        } else {
            response = await client.post(`notes`, note);
        }
        return response.json.data;
    }
}

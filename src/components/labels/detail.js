import { inject } from 'aurelia-framework';
import { LabelService } from '../../services/label';
import { NoteService } from '../../services/note';

@inject(LabelService, NoteService)
export class LabelDetail{
    constructor(LabelService, NoteService){
        this.message = "Label Detail Page";
        this.label = {};
        this.LabelService = LabelService;
        this.NoteService = NoteService;
    }
    activate(params, routeConfig, navigationInstruction) {
        return this.getLabel(params.id);
    }
    async getLabel(id) {
        this.label = await this.LabelService.getById(id);
        const notes = await this.NoteService.getAll();
        this.notes = notes.filter(note => note.labels.indexOf(id) !== -1);
    }
}

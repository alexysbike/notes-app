import { inject } from 'aurelia-framework';
import { LabelService } from '../../services/label';

@inject(LabelService)
export class LabelList {
    constructor(LabelService) {
        this.message = "Labels List Page";
        this.labels = [];
        this.LabelService = LabelService;
    }
    activate(params, routeConfig, navigationInstruction) {
        return this.getLabels();
    }
    async getLabels() {
        this.labels = await this.LabelService.getAll();
    }
}
import { inject } from 'aurelia-framework';
import { LabelService } from '../../services/label';

@inject(LabelService)
export class NoteForm {
    constructor(LabelService) {
        this.message = 'Label Form';
        this.label = {};
        this.LabelService = LabelService;
    }
    activate(params, routeConfig, navigationInstruction) {
        return this.getLabel(params.id);
    }
    async getLabel(id) {
        if (!id || id === 'new') {
            this.label = { label: '' };
        } else {
            this.label = await this.LabelService.getById(id);
        }
    }
    async save() {
        const response = await this.LabelService.save(this.label._id, this.label);
        this.label = response;
    }
}
import { inject } from 'aurelia-framework';
import { NoteService } from '../../services/note';

@inject(NoteService)
export class NoteDetail{
    constructor(NoteService){
        this.message = "Note Detail Page";
        this.note = {};
        this.NoteService = NoteService;
    }
    activate(params, routeConfig, navigationInstruction) {
        return this.getNote(params.id);
    }
    async getNote(id) {
        this.note = await this.NoteService.getById(id);
    }
}

import { inject } from 'aurelia-framework';
import { NoteService } from '../../services/note';
import { LabelService } from '../../services/label';

@inject(NoteService, LabelService)
export class NoteForm {
    constructor(NoteService, LabelService) {
        this.message = 'Note Form';
        this.note = {};
        this.labels = [];
        this.NoteService = NoteService;
        this.LabelService = LabelService;
    }
    activate(params, routeConfig, navigationInstruction) {
        this.getLabels();
        return this.getNote(params.id);
    }
    async getNote(id) {
        if (!id || id === 'new') {
            this.note = { note: '', labels: [] };
        } else {
            this.note = await this.NoteService.getById(id);
            this.note.labels = this.note.labels.map(label => label._id);
        }
    }
    async getLabels(){
        this.labels = await this.LabelService.getAll();
    }
    async save() {
        const response = await this.NoteService.save(this.note._id, this.note);
        this.note = response;
    }
}
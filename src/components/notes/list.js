import { inject } from 'aurelia-framework';
import { NoteService } from '../../services/note';
import Store from '../../store';
@inject(NoteService, Store)
export class NoteList {
    constructor(NoteService, Store) {
        this.message = "Notes List Page";
        this.noteService = NoteService;
        this.store = Store;
        const state = Store.getState();
        this.notes = state.notes.data;
    }
    activate(params, routeConfig, navigationInstruction) {
        return this.getNotes();
    }
    async getNotes() {
        await this.noteService.syncData();
        const state = this.store.getState();
        this.notes = state.notes.data;
        return true;
    }
}